import sys
import platform
from setuptools import setup, find_packages

if sys.version_info[0] < 3:
    raise Exception(
        'Are you trying to install QuimicPy on the python version {}.\n'
        'Please install QuimicPy in Python 3 instead.'.format(
            platform.python_version()
        )
    )

#Get information dynamically
QUIMICPY = __import__("quimicpy")
VERSION = QUIMICPY.__version__
AUTHOR = QUIMICPY.__author__

with open('README.md') as f:
    LONG_DESCRIPTION = f.read()

setup(
    name='QuimicPy',
    version=VERSION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    python_requires='>=3.4, <4',
    packages=['quimicpy']
)

# encoding: utf-8
import json, sys, os
import re


class QuimicPy():
    '''
    Uma api voltada em auciliar na parte de quimica.
    Trazendo inforaÃ§oes sobre os elemetos e realizando calculos desejados
    '''
    def __init__(self, file_name="elements.json", lang="pt_br", temp_unit="C"):
        self.file_name = file_name
        self.lang = lang
        self.temp_unit = temp_unit

        if (self.file_name in os.listdir(os.getcwd())) == False:
            self.create_file_of_elements()

        arch = open(f"{os.path.dirname(__file__)}/{self.file_name}", "r", encoding='utf8')
        self.elements = json.load(arch)
        arch.close()

        self.index = {}

        for element in self.elements:
            current = self.elements[element]
            self.index[current["symbol"]] = element

    def create_file_of_elements(self):
        '''
        Cria o arquivo com as informaÃ§oáº½s do dos elementos
        '''
        path = os.path.abspath(os.path.dirname(__file__))
        data = open(f"{path}/elements.json", "r", encoding='utf8')
        cp = json.load(data)
        data.close()
        data = open(self.file_name, "w", encoding='utf8')
        json.dump(cp, data, indent=1, ensure_ascii=False)
        data.close()

    def get_info_of_element(self, element):
        '''
        Pega as informaÃ§Ãµes dos elementos e retorna um dicionaro com as informaÃ§oes
        '''
        if element in self.index:
            return self.elements[self.index[element]]
        else:
            return None

    def calculator(self, obj, rounded=False):
        reg = re.findall(r'([A-Z][a-z]*)(\d*)', obj)
        obj_element = {}
        for elements in reg:
            atom = elements[0]
            if elements[1] == '':
                num_atom = 1
            else:
                num_atom = int(elements[1])

            if atom in self.index:
                weight = self.elements[self.index[atom]]["atomic_weight"][0]
                try:
                    if obj_element[atom] != None:
                        obj_element[atom] = obj_element[atom] + (weight * num_atom)
                except KeyError:
                    obj_element[atom] = weight * num_atom

        total_weight = 0

        for num in obj_element:

            total_weight += obj_element[num]
        if rounded:
            obj_element["total_weight"] = round(total_weight, 2)

        else:
            obj_element["total_weight"] = total_weight




        return obj_element

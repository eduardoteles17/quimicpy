# QuimicPy

### Português 
Uma biblioteca em python focada em auxiliar no aprendizado em quimica

#### Requerimentos
* [Python 3.6](https://www.python.org/downloads/) ou superior
* [setuptools](https://pypi.org/project/setuptools/)
* [git](https://git-scm.com/downloads)

#### Como instalar

Esse tutorial é de como instalar no linux

```

git clone https://gitlab.com/eduardoteles/quimicpy.git

cd quimicpy

python3 setup.py build

sudo python3 setup.py install

```

#### Como utilizar
```
from quimicpy import QuimicPy

quimicpy = QuimicPy()

#Pega as informaçoes por meio do simbolo dele e retorna um Objeto
quimicpy.get_info_of_element("H")

#Realiza a soma de todas a massas retorna um Objeto. Obs: parenteses não são suportados
quimicpy.calculator("CO2")
```

# Licença
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
